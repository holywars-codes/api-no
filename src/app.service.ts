import { Injectable } from '@nestjs/common';
import { EntityManager } from '@mikro-orm/postgresql';
import { EntityDTO, serialize } from '@mikro-orm/core';
import { PostEntity } from './entities/post.entity';

@Injectable()
export class AppService {
  constructor(private readonly em: EntityManager) {}
  async getPostById(id: number): Promise<EntityDTO<PostEntity>> {
    const post = await this.em.findOneOrFail(PostEntity, { id });
    return serialize(post);
  }
}
