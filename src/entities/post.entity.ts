import { Entity, PrimaryKey, Property } from '@mikro-orm/core';

@Entity({ schema: 'public', tableName: 'posts' })
export class PostEntity {
  @PrimaryKey()
  id: number;

  @Property({ type: 'varchar' })
  title: string;

  @Property({ type: 'varchar' })
  body: string;

  @Property({ type: 'bool' })
  published: boolean;
}
